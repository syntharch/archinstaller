#!/bin/bash
mkdir ~/tmp
cd ~/tmp
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

#How to use it?#

#First, install the primary key - it can then be used to install our keyring and mirrorlist.#

sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com

sudo pacman-key --lsign-key FBA220DFC880C036

sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

#Append (adding to the end of the file) to /etc/pacman.conf:#

echo [chaotic-aur]
sleep 1
echo Include = /etc/pacman.d/chaotic-mirrorlist# 
sleep 3